/*
    This file is part of the dandelion*.

    dandelion* is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    dandelion* is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with the dandelion*.

    If not, see <http://www.gnu.org/licenses/>.
 */
package com.dfa.hubzilla_android.data;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

public class DiasporaAspect {
    public long id;
    public String name;
    public boolean selected;


    public DiasporaAspect(JSONObject json) throws JSONException {
        if (json.has("id")) {
            id = json.getLong("id");
        }
        if (json.has("name")) {
            name = json.getString("name");
        }
        if (json.has("selected")) {
            selected = json.getBoolean("selected");
        }
    }

    @Override
    public String toString() {
        return toShareAbleText();
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof DiasporaAspect && ((DiasporaAspect) o).id == id;
    }

    public String toShareAbleText() {
        return String.format(Locale.getDefault(), "%d%%%d%%%s", selected ? 1 : 0, id, name);
    }
}
